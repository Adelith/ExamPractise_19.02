﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessGameExam 
{
    public class TurnChecker
    {
        public static void MistakesChecker(Figure figure)
        {
            int numberStart = figure.start[1];
            int numberEnd = figure.end[1];
            if (!(figure.colour == "white" || figure.colour == "black")) throw new Exception("There is no such colour");
            if (!(numberStart >= 1 || numberStart <= 8)) throw new Exception("There is no such position");
            if (!(numberEnd >= 1 || numberEnd <= 8)) throw new Exception("There is no such position");
        }

        public static void FinalCheck(Figure figure, int i)
        {

            bool flag = true;
            string name = figure.name;
            string letterStart = figure.start[0].ToString();
            string letterEnd = figure.end[0].ToString();
            int numberStart = figure.start[1];
            int numberEnd = figure.end[1];
            MistakesChecker(figure);
            int numStart = 0;
            int numEnd = 0;
            numStart = LetInt.LetterInter(letterStart);
            numEnd = LetInt.LetterInter(letterEnd);
            switch (name)
            {
                case "pawn":
                    if ((letterStart == letterEnd) && (numberStart == 2 && (numberEnd - numberStart) <= 2 || (numberEnd - numberStart == 1))) flag = true;
                    break;
                case "queen":
                    if ((letterStart == letterEnd && numberStart != numberEnd) || (numberStart == numberEnd && letterEnd != letterStart) && ((numberEnd - numberStart) <= 7 && letterEnd != letterStart && (numEnd - numStart) <= 7)) flag = true;
                    break;
                case "knight":
                    if ((numEnd - numberStart) == 1 && ((numberEnd - numberStart) == 2) || ((numEnd - numberStart) == -1) && (numberEnd - numberStart) == 2 || (numEnd - numberStart) == 2 && (numberEnd - numberStart) == 1 || (numEnd - numberStart) == 2 && (numberEnd - numberStart) == -1) flag = true;
                    break;
                case "bishop":
                    if ((numberEnd - numberStart) == (numEnd - numStart) && ((numberEnd - numberStart) <= 7 || (numberEnd - numberStart) >= -7) && ((numEnd - numStart) <= 7 || (numEnd - numStart) >= -7)) flag = true;
                    break;
                case "rook":
                    if (letterStart == letterEnd && ((numberEnd - numberStart) <= 7 || (numberEnd - numberStart) >= -7)) flag = true;
                    break;
                case "king":
                    if ((numEnd - numStart) == 1 && letterStart != letterEnd) flag = true;
                    break;
                default:
                    throw new Exception("There is no figures with name like this one");
            }
            if (flag) Console.WriteLine("Turn is able");
            else Console.WriteLine("Turn is unable");
        }
    }
}
