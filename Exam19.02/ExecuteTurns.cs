﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessGameExam
{
    public class ExecuteTurns
    {
        public static List<Figure> Execute(string[] turns)
        {
            List<Figure> figures = new List<Figure>();
            for (int i = 0; i < turns.Length; i++)
            {
                string[] str = turns[i].Split(" ");
                Figure figure = new Figure();
                figure.colour = str[0].ToString();
                figure.name = str[1].ToString();
                figure.start = str[2].ToString();
                figure.end = str[3].ToString();
                figures.Add(figure);
            }
            return figures;
        }
    }
}
