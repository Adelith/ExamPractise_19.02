﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ChessGameExam
{   
    public class Figure
    {
        public string colour { get; set; }
        public string name { get; set; }
        public string start { get; set; }
        public string end { get; set; }
    }
    

    


    public class Programm
    {   
        public static string Path = @"turns.txt";
        public static void Main()
        {
            string[] turns = File.ReadAllLines(Path);
            List<Figure> figures = ExecuteTurns.Execute(turns);
            for (int i = 0; i < figures.Count; i++)
            {
                TurnChecker.FinalCheck(figures[i], i);
            }
        }
    }
}

