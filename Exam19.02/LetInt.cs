﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessGameExam
{
    public static class LetInt
    {
        public static int LetterInter(string letter)
        {
            int a = 0;
            switch (letter)
            {
                case "a":
                    a = 1;
                    break;
                case "b":
                    a = 2;
                    break;
                case "c":
                    a = 3;
                    break;
                case "d":
                    a = 4;
                    break;
                case "e":
                    a = 5;
                    break;
                case "f":
                    a = 6;
                    break;
                case "g":
                    a = 7;
                    break;
                case "h":
                    a = 8;
                    break;
            }
            return a;
        }
    }
}
